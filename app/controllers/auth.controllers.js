const {users} = require('../database/models');
const jwt = require('jsonwebtoken');
const pwdHash = require('password-hash');

const method = {};

method.registrasi = async (req, res) => {
    try {
        const reqEmail = req.body.email;
        const email = reqEmail.toLowerCase();

        if(email){
            const cekEmail = await users.findOne({ where: { email: email } });
            if(!cekEmail){
                const newUser = await users.create({
                    email: email,
                    password: pwdHash.generate(req.body.password),
                    name: req.body.name,
                    tlp: req.body.tlp,
                    addrees: req.body.addrees
                });
                const token = jwt.sign({
                    id: newUser.id
                }, process.env.SECRET_KEY);

                res.header('Authorization', token);
                res.status(200).json({
                    statusCode: 200,
                    statusText: 'success',
                    message: 'Registration successfully',
                    token: token
                });
            } else {
                res.status(400).send({
                    statusCode: 400,
                    message: 'Email already registered'
                });
            }
        }
    } catch (error) {
        res.status(500).send(error);
    }
}

method.login = async (req, res) => {
    try {
        const reqEmail = req.body.email;
        const email = reqEmail.toLowerCase();
        const dtUser = await users.findOne({ where: { email: email } });

        if(dtUser){
            const verify = await pwdHash.verify(req.body.password, dtUser.password);
            if(verify === true){
                const token = jwt.sign({
                    id: dtUser.id
                }, process.env.SECRET_KEY);

                res.header('Authorization', token);
                res.status(200).json({
                    statusCode: 200,
                    statusText: 'success',
                    message: 'Login successfully',
                    token: token
                });
            } else {
                res.status(404).send({
                    statusCode: 404,
                    message: 'Wrong password!'
                });
            }
        } else {
            res.status(404).send({
                statusCode: 404,
                message: 'Email not registered, Please create an account first.'
            });
        }
    } catch (error) {
        res.status(500).send(error);
    }
}

module.exports = method;