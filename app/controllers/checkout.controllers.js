const { transactions, products } = require('../database/models');
const redis = require('redis');
const method = {};

const client = redis.createClient({
    host: '127.0.0.1',
    port: 6379,
});

method.checkout = (req, res) => {
    try {
        const id_user = req.token.id;

        //Get data dari redis
        client.hgetall(id_user, async (err, results) => {
            const data = JSON.parse(results.basket);
            let total_price = 0;

            //Ngurangin stock dan ambil total purchases
            for(let i=0; i<data.length; i++){
                total_price += data[i].price * data[i].quantity;
                const id_prod = data[i].id_product;
                const quantity = data[i].quantity;

                const dtProducts = await products.findAll({where: {id: id_prod} });
                const sourceStock = await dtProducts.map((x) => x.stock);
                const stock = Number(sourceStock);
                const reduceStock = stock - quantity;

                await products.update({stock: reduceStock}, {
                    where: {id: id_prod}
                });
            }

            //Simpan dari cart redis ke database
            const dtPurchases = await transactions.create({
                id_user: id_user,
                item_purchases: data,
                total_purchases: total_price
            });

            //Delete data cart yang di redis
            client.del(id_user);

            res.status(200).json({
            statusCode: 200,
            statusText: 'success',
            message: 'Checkout transaction successfully',
            data: dtPurchases
        });
    });
    } catch (error) {
        res.send(error.message);
    }   
}

module.exports = method;