const {products} = require('../database/models');
const method = {};

method.createProduct = async (req, res) => {
    try {
        const dataProduct = {...req.body};
        await products.create(dataProduct);

        res.status(200).json({
            statusCode: 200,
            statusText: 'success',
            message: 'Create Product successfully'
        });
    } catch (error) {
        res.status(500).send(error);
    }
}

module.exports = method;