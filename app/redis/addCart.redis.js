const redis = require('redis');
const method = {};

const client = redis.createClient({
    host: '127.0.0.1',
    port: 6379,
});

method.addItem = (req, res) => {
    try {
        const id_user = req.token.id;
        const basket = req.body.basket;

        client.hgetall(id_user, (err, results) => {
            if(results === null){
                const newCart = [basket];
                client.hset(id_user, "basket", JSON.stringify(newCart));

                res.status(200).json({
                    statusCode: 200,
                    statusText: 'success',
                    message: 'add Item successfully'
                });
            } else {
                let newCart = JSON.parse(results.basket);
                newCart.push(basket);
                client.hset(id_user, "basket", JSON.stringify(newCart));
                
                res.status(200).json({
                    statusCode: 200,
                    statusText: 'success',
                    message: 'add Item successfully'
                });
            }
        });
    } catch (error) {
        res.send(error.message);
    }
}

module.exports = method;