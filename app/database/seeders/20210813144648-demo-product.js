'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.bulkInsert('products', [
      {
        name_product: 'a',
        price: 1000,
        stock: 10
      },
      {
        name_product: 'b',
        price: 2000,
        stock: 20
      },
      {
        name_product: 'c',
        price: 3000,
        stock: 30
      }
    ], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
