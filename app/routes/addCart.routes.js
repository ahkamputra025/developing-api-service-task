const router = require('express').Router();

const addCartRedis = require('../redis/addCart.redis');
const authMiddleware = require('../middleware/auth.middleware');

router.post('/addItem', authMiddleware.checkToken, addCartRedis.addItem);

module.exports = router;