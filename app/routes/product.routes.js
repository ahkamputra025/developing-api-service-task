const router = require('express').Router();

const productController = require('../controllers/product.controller');
const authMiddleware = require('../middleware/auth.middleware');

router.post('/product', authMiddleware.checkToken, productController.createProduct);

module.exports = router;