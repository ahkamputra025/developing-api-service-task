const router = require('express').Router();

const checkoutController = require('../controllers/checkout.controllers');
const authMiddleware = require('../middleware/auth.middleware');

router.get('/checkout', authMiddleware.checkToken, checkoutController.checkout);

module.exports = router;