require('dotenv').config();
const cors = require('cors');
const express = require('express');

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

const authRoutes = require('./routes/auth.routes');
const productRoutes = require('./routes/product.routes');
const addCartRoutes = require('./routes/addCart.routes');
const checkoutRoutes = require('./routes/checkout.routes');

app.use('/takalab', authRoutes);
app.use('/takalab', productRoutes);
app.use('/takalab', addCartRoutes);
app.use('/takalab', checkoutRoutes);

app.all('*', (req, res) => {
    res.send('Are you lost...!?');
});

const PORT = process.env.PORT || 3333;

app.listen(PORT, async () => {
    console.log(`Server up on http://localhost:${PORT}`);
});