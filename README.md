# Documentation
Takalab Back-End (BE) Developer Test

# Getting started
1. Open `Redis` server
2. Move yourself to the backend folder: `cd app`
3. Copy the .env.Example file and create a .env file and add the connection DB and other and SIGNATURE (can be any word)
4. Install sequelize-cli how dev-dependency and execute this `sequelize db:create` next `sequelize db:migrate`
5. Install node-modules `$ npm i` and run with command `$ npm start`

# API List
service ecommerce API

| Routes | EndPoint             | Description                                            |
| ------ | -------------------- | ------------------------------------------------------ |
| POST   | /takalab/register    | Register user and get token for authentication          |
| POST   | /takalab/login       | login user and get token for authentication             |
| POST   | /takalab/addItem     | Add product to cart and saved in redis                 |
| GET    | /takalab/checkout/   | Data from redis is deleted and stored in database,     |
|        |                      | and reduces stock in product master                    |

```
POST
/takalab/register
email: {string}, form-encoded
password: {string}, form-encoded
name: {string}, form-encoded
tlp: {string}, form-encoded
addrees: {string}, form-encoded

POST
/takalab/login
email: {string}, form-encoded
password: {string}, form-encoded

POST
/takalab/addItem
(form-raw)
"id_user": 1,
"basket": {"id_product": 1, "price": 1000, "quantity": 10}

GET
/takalab/checkout/
(empty-form)
```
